import { Action } from 'src/app/shared/models/action';

export enum AmadeusActionTypes {
    LOAD_IATA_AIRPORTS = 'LOAD_IATA_AIRPORTS',
    LOW_FARE_FLIGHTS_SEARCH = 'LOW_FARE_FLIGHTS_SEARCH',
    FLIGHTS_OFFERS_SEARCH = 'FLIGHTS_OFFERS_SEARCH',
    HOTEL_SEARCH = 'HOTEL_SEARCH'
}

export class LoadIataAirports implements Action {
    readonly type = AmadeusActionTypes.LOAD_IATA_AIRPORTS;
    constructor(public payload: any) {}
}
export class LowFareFlightsSearch implements Action {
    readonly type = AmadeusActionTypes.LOW_FARE_FLIGHTS_SEARCH;
    constructor(public payload: any) {}
}


export class FlightsOffersSearch implements Action {
    readonly type = AmadeusActionTypes.FLIGHTS_OFFERS_SEARCH;
    constructor(public payload: any) { console.log(payload); }
}
export class HotelSearch implements Action {
    readonly type = AmadeusActionTypes.HOTEL_SEARCH;
    constructor(public payload: any) {}
}

export type AmadeusActions = LoadIataAirports | LowFareFlightsSearch | HotelSearch | FlightsOffersSearch;
