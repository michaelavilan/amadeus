import { AmadeusActionTypes, AmadeusActions } from './actions';

export let initialState = [];

export function reducer(state = initialState, action: AmadeusActions) {
    switch (action.type) {
        case AmadeusActionTypes.LOAD_IATA_AIRPORTS:
            return [...state, action];
        case AmadeusActionTypes.LOW_FARE_FLIGHTS_SEARCH:
            return [...state, action];
        case AmadeusActionTypes.HOTEL_SEARCH:
            return [...state, action];
        case AmadeusActionTypes.FLIGHTS_OFFERS_SEARCH:
            return [...state, action];
        default:
            return state;
    }
}
