import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { SessionService } from '../session/session.service';
import { Localtoken } from 'src/app/shared/models/localtoken';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  SERVICE_DEFNITION: any;
  constructor ( private restangular: Restangular, private session: SessionService ) { }
  public getAccessCredentials ( apiName: string, tokenData: any, endpoint: any, success: any, fault: any ) {
    const localToken: Localtoken = new Localtoken();
    let data: any;
    let centinela = false;
    data = this.session.getLocalValue('session', apiName + '_auth_credentials');
    if ( data ) {
      data = JSON.parse( data );
      if ( this.validateTimestamp(data.timeStamp, data.expires_in ) ) {
        centinela = true;
        success( data );
      } else {
        centinela = false;
        this.resetToken();
      }
    }

    if ( centinela === false ) {
      this.restangular.withConfig(( RestangularConfigurer: any ) => {
        RestangularConfigurer.setBaseUrl( endpoint.path );
      }).all('token').post(
        'grant_type=' + tokenData.grantType +
        '&client_id=' + tokenData.clientId +
        '&client_secret='  + tokenData.clientSecret,
        undefined, { 'Content-Type': 'application/x-www-form-urlencoded' }
      ).subscribe(
        res => {
          const TIMESTAMP = new Date();
          localToken.access_token = res.access_token;
          localToken.expires_in = res.expires_in;
          localToken.token_type = res.token_type;
          localToken.timeStamp = TIMESTAMP.toUTCString();
          this.session.setLocalValue
          (
            'session',
            { key: apiName + '_auth_credentials', value: JSON.stringify( localToken ) }
          );
          success( localToken );
        },
        err => {
          fault(err);
        }
      );
    }
  }
  public resetToken () {
    this.session.deleteAllLocalData ( 'session' );
  }
  private validateTimestamp (timeStamp: string, expiresIn: number): boolean {
    const ts: Date = new Date(timeStamp);
    const current: Date = new Date();
    const secconds: any = ( current.getTime() - ts.getTime() ) / 1000;
    const result: any = expiresIn - secconds;
    console.log(result);
    if ( result > 0 ) {
      return true;
    } else {
      return false;
    }
  }
}
