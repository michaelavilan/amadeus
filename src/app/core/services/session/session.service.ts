import { Injectable } from '@angular/core';
import { Session } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor() { }
  public setLocalValue ( use: string, data: any ) {
    switch ( use ) {
      case 'session':
       if (window.sessionStorage) {
         sessionStorage.setItem(data.key, data.value);
       } else {
         console.log('error');
       }
      break;
      case 'local':
      if (window.localStorage) {
        localStorage.setItem(data.key, data.value);
      } else {
        console.log('error');
      }
      break;
    }
  }
  public getLocalValue ( use: string, key: string ): any {
    let localValue: any;
    switch ( use ) {
      case 'session':
      localValue = sessionStorage.getItem(key);
      break;
      case 'local':
      localValue = localStorage.getItem(key);
      break;
    }
    return localValue;
  }
  public deleteLocalValue ( use: string, key: string ) {
    switch ( use ) {
      case 'session':
      break;
      case 'local':
      localStorage.removeItem(key);
      break;
    }
  }
  public deleteAllLocalData ( use: string ) {
    switch ( use ) {
      case 'session':
      break;
      case 'local':
      localStorage.clear();
      break;
    }
  }
}
