import { TestBed } from '@angular/core/testing';

import { RestConfigurationService } from './rest-configuration.service';

describe('RestConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestConfigurationService = TestBed.get(RestConfigurationService);
    expect(service).toBeTruthy();
  });
});
