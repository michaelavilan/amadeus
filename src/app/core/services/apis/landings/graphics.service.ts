import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class GraphicsService {

  constructor(private restangular: Restangular) { }
  getStatsByContext (
    university: string,
    column: string,
    status: string,
    context: string,
    init: Date,
    end: Date,
    landingName: string,
    type: string,
    success: any,
    fault: any
  ) {}
  getTotalStats (
    university: string,
    column: string,
    status: string,
    context: string,
    init: Date,
    end: Date,
    landingName: string,
    success: any,
    fault: any
  ) {}
  getLeadStatus ( guid: string, success: any, fault: any ) {}
}
