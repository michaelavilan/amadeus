import { TestBed } from '@angular/core/testing';

import { SiafService } from './siaf.service';

describe('SiafService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiafService = TestBed.get(SiafService);
    expect(service).toBeTruthy();
  });
});
