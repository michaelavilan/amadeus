import { TestBed } from '@angular/core/testing';

import { BtlService } from './btl.service';

describe('BtlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BtlService = TestBed.get(BtlService);
    expect(service).toBeTruthy();
  });
});
