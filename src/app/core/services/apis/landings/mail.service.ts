import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private restangular: Restangular) { }
  send (
    mailFrom: any,
    repplyTo: any,
    addresses: any,
    subject: string,
    body: string,
    text: string,
    success: any,
    fault: any
  ) {}
}
