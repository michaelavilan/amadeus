import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { IAppConfig } from 'src/app/shared/models/app-config.model';
import { RestConfigurationService } from '../rest-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class CrmSupportingService {
  SERVICE_DEFINITION: any;
  constructor(private restangular: Restangular, private restConfig: RestConfigurationService) { }
  public getPrograms (
    config: IAppConfig,
    university: string,
    success: any,
    fault: any,
    programType: string = null,
    modality: string = null,
    opportunityType: string = null
    ) {
      this.setDefinition( config );
      let FD: FormData;
      FD = new FormData();
      FD.append('service', '1');
      FD.append('university', university);
      if ( modality != null ) {
        FD.append('modal', modality);
      }
      if ( programType != null ) {
        FD.append('type', programType);
      }
      if ( opportunityType != null ) {
        FD.append('typeOpp', opportunityType);
      }
      this.SERVICE_DEFINITION.CONSUMER.one('').customPOST(FD, '', undefined, {'Content-Type': undefined}).subscribe(
        res => {
          success(res);
        },
        err => {
          fault(err);
        }
      );
  }
  public getProvinces ( config: IAppConfig, country: string, success: any, fault: any ) {
    let FD: FormData;
    FD = new FormData();
    FD.append('service', '3');
    FD.append('country', country);
    this.setDefinition( config );
    this.SERVICE_DEFINITION.CONSUMER.one('').customPOST(FD, '', undefined, {'Content-Type': undefined}).subscribe(
      res => {
        success(res);
      },
      err => {
        fault(err);
      }
    );
  }
  public getCities ( config: IAppConfig, country: string, province: string, success: any, fault: any ) {
    let FD: FormData;
    FD = new FormData();
    FD.append('service', '4');
    FD.append('country', country);
    FD.append('state', province);
    this.setDefinition( config );
    this.SERVICE_DEFINITION.CONSUMER.one('').customPOST(FD, '', undefined, {'Content-Type': undefined}).subscribe(
      res => {
        success(res);
      },
      err => {
        fault(err);
      }
    );
  }
  public getDocumentTypes ( config: IAppConfig, country: string, success: any, fault: any ) {
    let FD: FormData;
    FD = new FormData();
    FD.append('service', '7');
    FD.append('country', country);
    this.setDefinition( config );

     this.SERVICE_DEFINITION.CONSUMER.one('').customPOST(FD, '', undefined, {'Content-Type': undefined}).subscribe(
      res => {
        success(res);
      },
      err => {
        fault(err);
      }
    );
  }
  private setDefinition ( config: IAppConfig ) {
    if ( this.SERVICE_DEFINITION === undefined ) {
      this.SERVICE_DEFINITION = this.restConfig.getLandingsConfig( { api: 'landings', endpoint: 'rest' }, config );
    }
  }
}
