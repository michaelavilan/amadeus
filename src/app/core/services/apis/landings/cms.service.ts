import { Injectable } from '@angular/core';
import { RestConfigurationService } from '../rest-configuration.service';
import { IAppConfig } from '../../../../shared/models/app-config.model';

@Injectable({
  providedIn: 'root'
})
export class CMSService {
  SERVICE_DEFINITION: any;
  constructor(private restConfig: RestConfigurationService) {
  }
  public getContents (config: IAppConfig, success: any, fault: any ) {
    this.setDefinition( config );
    let formData: FormData;
    formData = new FormData();
    formData.append('service', '1');
    formData.append('forms', this.SERVICE_DEFINITION.WEBFORMS);
    this.SERVICE_DEFINITION.CONSUMER.one('').customPOST(
        formData, '', undefined, {'Content-Type': undefined}
      ).subscribe(
        function ( res: any ) {
          success(res);
        },
        function ( err: any ) {
          fault(err);
        }
      );
  }
  private setDefinition ( config: IAppConfig ) {
    if ( this.SERVICE_DEFINITION === undefined ) {
      this.SERVICE_DEFINITION = this.restConfig.getLandingsConfig( { api: 'landings', endpoint: 'cms' }, config );
    }
  }
}
