import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class SiafService {

  constructor(private restangular: Restangular) { }
  deleteFormRegs ( formID: string, success: any, fault: any ) {}
  insertReg ( adminID: string, formID: string, regContent: string, date: Date, success: any, fault: any ) {}
  getFormDefinition ( formID: string, success: any, fault: any ) {}
}
