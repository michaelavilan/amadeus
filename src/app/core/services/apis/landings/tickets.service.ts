import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private restangular: Restangular) { }
  getContact ( documentType: string, document: string, university: string, success: any, fault: any ) {}
  getAcademicRegisters ( contactGuid: string, university: string, success: any, fault: any ) {}
  getFormData ( formID: string, success: any, fault: any ) {}
  createNote (
    ticketID: string,
    university: string,
    note: string,
    fileName: string = 'N/A',
    mimeType: string = 'N/A',
    success: any,
    fault: any
   ) {}
   createTicket (
     university: string,
     document: string,
     email: string,
     description: string,
     subCategory: string,
     serviceType: string,
     academicRegister: string,
     success: any,
     fault: any
    ) {}
}
