import { TestBed } from '@angular/core/testing';

import { CrmSupportingService } from './crm-supporting.service';

describe('CrmSupportingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrmSupportingService = TestBed.get(CrmSupportingService);
    expect(service).toBeTruthy();
  });
});
