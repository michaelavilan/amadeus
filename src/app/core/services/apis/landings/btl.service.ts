import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class BtlService {

  constructor(private restangular: Restangular) { }
  login ( mail: string, password: string, success: any, fault: any ) {}
  socialGatewayLogin ( university: string, document: string, success: any, fault: any ) {}
  getStrategies ( university: string, success: any, fault: any ) {}
  insertRegister
  (
    userName: string,
    userLastName: string,
    userEmail: string,
    userMobilePhone: string,
    programType: string,
    modality: string,
    site: string,
    firstInterestProgram: string,
    habeasData: string,
    loadPresence: string,
    servicePresence: string,
    acceptanceDate: string,
    countryCode: string,
    universityCode: string,
    digitalLabDetails: string,
    registerType: string,
    campaignCode: string,
    opportunityType: string,
    medium: string,
    mediumDetail: string,
    landingContent: string,
    landingIP: string,
    landingMedium: string,
    landingName: string,
    landingSource: string,
    landingTerm: string,
    candidateType: string,
    userDocumentType: string = 'N/A',
    userDocumentNumber: string = 'N/A',
    userPhone: string = 'N/A',
    userLastGrade: string = 'N/A',
    userGenre: string = 'N/A',
    state: string = 'N/A',
    city: string = 'N/A',
    CSUAgent: string = 'N/A',
    agentName: string = 'N/A',
    agentDocument: string = 'N/A',
    agentMail: string = 'N/A',
    exploraName: string = 'N/A',
    exploraDocument: string = 'N/A',
    exploraMail: string = 'N/A',
    strategy: string = 'N/A',
    schoolName: string = 'N/A',
    secondInterestProgram: string = 'N/A',
    interestPeriod: string = 'N/A',
    address: string = 'N/A',
    neightborhood: string = 'N/A',
    linkedin: string = 'N/A',
    firstAttendantName: string = 'N/A',
    firstAttendantMail: string = 'N/A',
    firstAttendantPhone: string = 'N/A',
    keyPoint: string = 'N/A',
    twitter: string = 'N/A',
    facebook: string = 'N/A',
    activityOrigin: string = 'N/A',
    firstInterestArea: string = 'N/A',
    secondInterestArea: string = 'N/A',
    universityChosen: string = 'N/A',
    originalIndexRow: string = 'N/A',
    universityMedium: string = 'N/A',
    agreement: string = 'N/A',
    company: string = 'N/A',
    secondAttendantName: string = 'N/A',
    secondAttendantMail: string = 'N/A',
    secondAttendantPhone: string = 'N/A',
    contactFormMail: string = 'N/A',
    contactFormPhone: string = 'N/A',
    contactFormFax: string = 'N/A',
    siteLabel: string = 'N/A',
    journey: string = 'N/A',
    journey2: string = 'N/A',
    academicLevel: string = 'N/A',
    description: string = 'N/A',
    interestLevel: string = 'N/A',
    agreementType: string = 'N/A',
    formaIngresoBR: string = 'N/A',
    period: string = 'N/A',
    subperiod: string = 'N/A',
    success: any,
    fault: any
  ) {}
  getSchools ( country: string, success: any, fault: any ) {}
  getCandidateTypes ( university: string, success: any, fault: any ) {}
}
