import { Injectable } from '@angular/core';
import { IAppConfig } from 'src/app/shared/models/app-config.model';
import { RestConfigurationService } from '../rest-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class FligthsService {
  API_NAME: string;
  constructor( private config: RestConfigurationService ) {
    this.API_NAME = 'amadeus-test';
  }
  public searchByDestination
  (
    appConfig: IAppConfig,
    keyword: string,
    subType: string,
    countryCode: string,
    success: any,
    fault: any) {
      let CONSUMER: any = this.config;

      this.config.getRestconfig( { api: this.API_NAME }, appConfig ,
        'reference-data',
        function ( definition: any ) {
          CONSUMER = CONSUMER.getCustomConsumer
          (
            definition.endpoint,
            definition.access_token
          );
          CONSUMER.all('locations').customGET('', {
              keyword: keyword,
              countryCode: countryCode,
              subType: subType
          }).subscribe
          (
            res => {
              success(res);
            },
            err => {
              fault(err);
            }
          );
        },
        function ( error: any ) {
          fault(error);
        }
      );
  }
  public searchOffers
  (
    appConfig: IAppConfig,
    origin: string,
    destination: string,
    departureDate: string,
    returnDate: string,
    success: any,
    fault: any) {
      let CONSUMER: any = this.config;

      this.config.getRestconfig( { api: this.API_NAME }, appConfig ,
        'shopping',
        function ( definition: any ) {
          CONSUMER = CONSUMER.getCustomConsumer
          (
            definition.endpoint,
            definition.access_token
          );
          CONSUMER.all('flight-offers').customGET('', {
              origin: origin,
              destination: destination,
              departureDate: departureDate,
              returnDate: returnDate
          }).subscribe
          (
            res => {
              success(res);
            },
            err => {
              fault(err);
            }
          );
        },
        function ( error: any ) {
          fault(error);
        }
      );
  }
}
