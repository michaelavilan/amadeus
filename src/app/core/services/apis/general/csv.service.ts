import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CsvService {

  constructor( private http: HttpClient ) { }
  getCSVRows ( path: string, delimiter: string = ',', success: any, fault: any ) {
    this.http.get(path, {responseType: 'text'}).toPromise().then((response: any) => {
      let rows: any = this.getProcessedRows(response, delimiter);
      success(rows);
    }).catch((error: any) => {
      fault(error);
    });
  }
  private getProcessedRows (content: string, delimiter: string) {
    delimiter = (delimiter || ',');
    const objPattern = new RegExp
    (
      (
        '(\\' + delimiter + '|\\r?\\n|\\r|^)' +
        '(?:\'([^\']*(?:\'\'[^\']*)*)\'|' +
        '([^\'\\' + delimiter + '\\r\\n]*))'
      ), 'gi'
    );
    const arrData = [[]];
    let arrMatches = null;
    let strMatchedValue: any;
    while (arrMatches = objPattern.exec(content)) {
      const strMatchedDelimiter = arrMatches[1];
      if
      (
        strMatchedDelimiter.length &&
        (
          strMatchedDelimiter !== delimiter
        )
      ) {
        arrData.push([]);
      }
      if (arrMatches[2]) {
        strMatchedValue = arrMatches[2].replace
        (
          new RegExp('\'\'', 'g'), '\''
        );
      } else {
        strMatchedValue = arrMatches[3];
      }
      arrData[arrData.length - 1].push( strMatchedValue );
    }
    return (arrData);
  }
}
