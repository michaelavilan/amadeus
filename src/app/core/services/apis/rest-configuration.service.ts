import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { APIDefinitionModel } from '../../../shared/models/landing-api-definition';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RestConfigurationService {
  currentRoute: string;
  constructor
  (
    private restangular: Restangular,
    private router: Router,
    private auth: AuthService
  ) {
    this.router.events.subscribe((res) => {
      this.currentRoute = this.router.url;
    });
  }
  /**
   * @param  apiDefinition Object
   * @param  config Object
   */
  public getLandingsConfig ( apiDefinition: APIDefinitionModel, config: any ): any {
    let _config: any;
    config.apis.forEach(api => {
      if ( api.name === apiDefinition.api ) {
        api.endpoints.forEach(endpoint => {
          if ( endpoint.name === apiDefinition.endpoint ) {
            api.routes.forEach(route => {
              if ( this.currentRoute === route.path ) {
                _config = {
                  CONSUMER: this.restangular.withConfig(( RestangularConfigurer: any ) => {
                    RestangularConfigurer.setBaseUrl( endpoint.path );
                  }),
                  WEBFORMS: JSON.stringify(route.webforms)
                };
              }
            });
          }
        });
      }
    });
    return _config;
  }
  public getRestconfig ( apiDefinition: any, config: any, entityEndpoint: string, success: any, fault: any ) {
    config.apis.forEach(api => {
      if ( api.name === apiDefinition.api ) {
        api.endpoints.forEach(endpoint => {
          if ( endpoint.name === 'auth' ) {
            this.auth.getAccessCredentials(
              api.name,
              api.token,
              endpoint,
              function ( data: any ) {
                api.token['athorized'] = data;
                api.endpoints.forEach(entity => {
                  if ( entity.name === entityEndpoint ) {
                    data['endpoint'] = entity.path;
                  }
                });
                success(data);
              },
              function ( error: any ) {
                console.log(error);
              }
            );
          }
        });
      }
    });
  }
  public getCustomConsumer ( endpoint: string, bearer: string ) {
    const CONSUMER = this.restangular.withConfig(( RestangularConfigurer: any ) => {
      RestangularConfigurer.setBaseUrl( endpoint );
      RestangularConfigurer.setDefaultHeaders({'Authorization': 'Bearer ' + bearer});
    });
    return CONSUMER;
  }
}
