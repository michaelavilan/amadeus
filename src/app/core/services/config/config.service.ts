import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { IAppConfig } from '../../../shared/models/app-config.model';
import { RestConfigurationService } from '../apis/rest-configuration.service';
@Injectable()
export class AppConfig {
    static settings: IAppConfig;
    static SERVICE_DEFINITION: any;

    constructor(private http: HttpClient) {}
    load( ) {
        const jsonFile = `config/config.${environment.name}.json`;
        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response: IAppConfig) => {
               AppConfig.settings = <IAppConfig>response;
               resolve();
            }).catch((response: any) => {
               reject('Could not load file');
            });
        });
    }
}
