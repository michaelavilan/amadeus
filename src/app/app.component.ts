import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as Amadeus from 'src/app/core/store/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'template';
  constructor(private store: Store<any>) {
    this.store.dispatch
    (
      new Amadeus.LoadIataAirports
      (
        {
          path:  'assets/datasets/iata-airport-codes-complete.csv',
          delimiter: ','
        }
      )
    );
  }
}
