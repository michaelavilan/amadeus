import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { APP_INITIALIZER } from '@angular/core';

import { LeadsFormComponent } from './shared/components/leads-form/leads-form.component';
import { WidgetComponent } from './shared/components/amadeus/widget/widget.component';

const routes: Routes = [
  { path: '', component: WidgetComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor () {
    this.fillRoutes();
  }
  fillRoutes() {
  }
}
