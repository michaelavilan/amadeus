import { Action } from '@ngrx/store';

export enum CoreActionTypes {
  LOAD_CONFIGURATION_SUCCESS = 'LOAD_CONFIGURATION_SUCCESS',
  LOAD_CONFIGURATION_FAULT = 'LOAD_CONFIGURATION_FAULT',
  GET_LOCAL_CREDENTIALS_SUCCESS = 'GET_LOCAL_CREDENTIALS_SUCCESS',
  GET_LOCAL_CREDENTIALS_FAULT = 'GET_LOCAL_CREDENTIALS_FAULT',
  ACCESS_TOKEN_RESET = 'ACCESS_TOKEN_RESET',
  AUTHENTICATE_REST_API = 'AUTHENTICATE_REST_API',
  AUTHENTICATE_REST_API_SUCCESS = 'AUTHENTICATE_REST_API_SUCCESS',
  AUTHENTICATE_REST_API_FAULT = 'AUTHENTICATE_REST_API_FAULT',
}
/**
 * Action emitted when is correctly loaded the local configuration JSON
 */
export class LoadConfigurationSuccessAction implements Action {
  readonly type = CoreActionTypes.LOAD_CONFIGURATION_SUCCESS;
  constructor(public payload: any) {}
}
/**
 * Action emitted when the local configuration JSON loding process fails
 */
export class LoadConfigurationFaultAction implements Action {
  readonly type = CoreActionTypes.LOAD_CONFIGURATION_FAULT;
  constructor(public payload: any) {}
}
/**
 * Action emitted when the Rest API credentials can be loaded from LocalStorage
 */
export class GetLocalCredentialsSuccessAction implements Action {
    readonly type = CoreActionTypes.GET_LOCAL_CREDENTIALS_SUCCESS;
    constructor(public payload: any) {}
}
/**
 * Action emitted when the Rest API credentials can't be loaded from LocalStorage
 */
export class GetLocalCredentialsFaultAction implements Action {
    readonly type = CoreActionTypes.GET_LOCAL_CREDENTIALS_FAULT;
    constructor(public payload: any) {}
}

export class AccessTokenRequestAction implements Action {
    readonly type = CoreActionTypes.ACCESS_TOKEN_RESET;
    constructor(public payload: any) {}
}

export class AuthenticateRestAPIAction implements Action {
    readonly type = CoreActionTypes.AUTHENTICATE_REST_API;
    constructor(public payload: any) {}
}

export class AuthenticateRestAPISuccessAction implements Action {
    readonly type = CoreActionTypes.AUTHENTICATE_REST_API_SUCCESS;
    constructor(public payload: any) {}
}

export class AuthenticateRestAPIFaultAction implements Action {
    readonly type = CoreActionTypes.AUTHENTICATE_REST_API_FAULT;
    constructor(public payload: any) {}
}

export type CoreActions =
LoadConfigurationSuccessAction |
LoadConfigurationFaultAction |
GetLocalCredentialsSuccessAction |
GetLocalCredentialsFaultAction |
AccessTokenRequestAction |
AuthenticateRestAPIAction |
AuthenticateRestAPISuccessAction |
AuthenticateRestAPIFaultAction;
