import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppStoreModule } from './app-store/app-store.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AppStoreModule
  ]
})
export class RootStoreModule { }
