import { Component, OnInit, Input } from '@angular/core';
import { CMSService } from '../../../core/services/apis/landings/cms.service';
import { CrmSupportingService } from '../../../core/services/apis/landings/crm-supporting.service';
import { FligthsService } from 'src/app/core/services/apis/amadeus/fligths.service';
import { AppConfig } from 'src/app/core/services/config/config.service';


@Component({
  selector: 'app-leads-form',
  templateUrl: './leads-form.component.html',
  styleUrls: ['./leads-form.component.css'],
  providers: [ AppConfig ]
})
export class LeadsFormComponent implements OnInit {

  @Input() locations: Array< any >;

  constructor(
    private cms: CMSService,
    private crm: CrmSupportingService,
    private fligths: FligthsService
  ) { }

  ngOnInit() {
    this.getIlumnoData();
  }
  private getAmadeusData () {
  }
  private getIlumnoData () {
    this.fligths.searchByDestination
    (
      AppConfig.settings,
      'MUC',
      'CITY,AIRPORT',
      'DE',
      data => {
        this.locations = data.data;
        console.log(this.locations);
      },
      error => { console.log(error); }
    );
    /*this.cms.getContents
      (
       AppConfig.settings,
       this.getContentsSuccess,
       this.getContentsFault
      );
      this.crm.getPrograms(AppConfig.settings, 'POLI', this.getProgramsSuccess, this.getProgramsFault);
      this.crm.getProvinces(AppConfig.settings, 'CO', this.getProvincesSuccess, this.getProvincesFault);
      this.crm.getCities(AppConfig.settings, 'CO', 'CO-AMA', this.getCitiesSuccess, this.getCitiesFault);
      this.crm.getDocumentTypes(AppConfig.settings, 'CO', this.getDocumentsSuccess, this.getDocumentsFault);*/
  }
  private getContentsSuccess ( data: any ) {
    console.log(data);
  }
  private getContentsFault ( error: any ) {}
  private getProgramsSuccess ( data: any ) {
    console.log(data);
  }
  private getProgramsFault ( error: any ) {}
  private getProvincesSuccess ( data: any ) {
    console.log(data);
  }
  private getProvincesFault ( error: any ) {}
  private getCitiesSuccess ( data: any ) {
    console.log(data);
  }
  private getCitiesFault ( error: any ) {}
  private getDocumentsSuccess ( data: any ) {
    console.log(data);
  }
  private getDocumentsFault ( error: any ) {}

}
