import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import * as Amadeus from 'src/app/core/store/actions';
import { CsvService } from 'src/app/core/services/apis/general/csv.service';
import { Observable } from 'rxjs';
import {NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { FligthsService } from 'src/app/core/services/apis/amadeus/fligths.service';
import { AppConfig } from 'src/app/core/services/config/config.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css'],
  providers: [ AppConfig ]
})
export class WidgetComponent implements OnInit {

  public origin: any;
  public destiny: any;
  public startDate: any;
  public endDate: any;
  public iataCodes: any;
  @Input() promos: Array< any >;

  constructor(private store: Store<any>, private csv: CsvService,
    private config: NgbTypeaheadConfig, private fligths: FligthsService) {}

  ngOnInit() {
    this.store.select('appState').subscribe
    (
      (
        state => this._setState(state)
      )
    );
  }

  searchOffers () {
    if ( 
      this.origin !== undefined &&
      this.destiny !== undefined &&
      this.startDate  !== undefined &&
      this.endDate !== undefined
      ) {
      this.store.dispatch
      (
        new Amadeus.FlightsOffersSearch
        (
          {
            origin: this.origin.iata_code,
            destiny: this.destiny.iata_code,
            startDate: this.startDate,
            endDate: this.endDate
          }
        )
      );
    } else {
      swal(
        'Ooops',
        'You must fill the form fields',
        'error'
      );
    }
  }

  private _setState (state: any) {
    console.log(state);
    console.log(this.origin);
    const current = state[state.length - 1];
    switch (current.type ) {
      case Amadeus.AmadeusActionTypes.LOAD_IATA_AIRPORTS:
      this.loadIataCodes(current.payload.path, current.payload.delimiter);
      break;
      case Amadeus.AmadeusActionTypes.LOW_FARE_FLIGHTS_SEARCH:
      break;
      case Amadeus.AmadeusActionTypes.FLIGHTS_OFFERS_SEARCH:
      this.fligths.searchOffers(
        AppConfig.settings,
        this.origin.iata_code,
        this.destiny.iata_code,
        this.startDate,
        this.endDate,
        data => {
          this.promos = data.data;
          swal.close();
          this.scrollTo( 'promos_div' );
         },
        error => {
          swal(
            'Ooops',
            'We have got an error: ' + error.message,
            'error'
          );
        }
      );
      break;
      case Amadeus.AmadeusActionTypes.HOTEL_SEARCH:
      console.log(3);
      break;
    }
  }

  private loadIataCodes ( path: string, delimiter: string ) {
    this.csv.getCSVRows
    (
      path, delimiter,
      data => {
        this.iataCodes = this.getFormatedIataList( data );
      },
      error => {
        swal(
          'Ooops',
          'We have got an error: ' + error.message,
          'error'
        );
      }
    );
  }

  private getFormatedIataList ( data: any ) {
    const items = [];
    data.forEach( iata => {
      if (
        String(iata[9]).length > 1 &&
        iata[1] === 'large_airport'
      ) {
        const item = {
          indent: iata[0],
          type: iata[1],
          name: iata[2],
          elevation_ft: iata[3],
          continent: iata[4],
          iso_country: iata[5],
          iso_region: iata[6],
          municipality: iata[7],
          gps_code: iata[8],
          iata_code: iata[9],
          local_code: iata[10],
          coordinates: iata[11],
          label: iata[2] + ', ' + iata[7] + ', ' + iata[5]
        };
        items.push(item);
      }
    });
    return items;
  }

  private scrollTo ($id: any) {
    const startY = currentYPosition();
    const stopY = elmYPosition($id);
    const distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 100) {
        scrollTo(0, stopY); return;
    }
    let speed = Math.round(distance / 100);
    if (speed >= 20) {
      speed = 20;
    }
    const step = Math.round(distance / 25);
    let leapY = stopY > startY ? startY + step : startY - step;
    let timer = 0;
    if (stopY > startY) {
        for (let i = startY; i < stopY; i += step) {
            setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
            leapY += step;
            if (leapY > stopY) {
              leapY = stopY; timer++;
            }
        } return;
    }
    for (let i = startY; i > stopY; i -= step) {
        setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
        leapY -= step; 
        if (leapY < stopY) {
          leapY = stopY; timer++;
        }
    }

    function currentYPosition() {
        if (self.pageYOffset) {
          return self.pageYOffset;
        }
        if (document.documentElement && document.documentElement.scrollTop) {
          return document.documentElement.scrollTop;
        }
        if (document.body.scrollTop) {
          return document.body.scrollTop;
        }

        return 0;
    }

    function elmYPosition($id: any) {
        const elm = document.getElementById($id);
        let y = elm.offsetTop;
        let node: any = elm;
        while (node.offsetParent && node.offsetParent !== document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        } return y;
    }
}

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.iataCodes.filter
        (
          v => v.label.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)
        )
    )
    formatter = (x: any) => x.name;
}
