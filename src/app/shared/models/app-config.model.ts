export interface IAppConfig {
    env: {
        name: string;
    };
    apis: {
        landings: {
            rest: string
            stats: string,
            siafApi: string,
            siafAssets: string
        }
    };
    analytics: {
        enabled: boolean
    };
    internalApps: Array < any >;
}
