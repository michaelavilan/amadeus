export interface APIDefinitionModel {
    api: string;
    endpoint: string;
}
