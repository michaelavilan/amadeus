import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
/* Template imports */
import { RestangularModule, Restangular } from 'ngx-restangular';
import { StoreModule } from '@ngrx/store';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { LeadsFormComponent } from './shared/components/leads-form/leads-form.component';
import { AppConfig } from './core/services/config/config.service';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { WidgetComponent } from './shared/components/amadeus/widget/widget.component';
import { reducer } from './core/store/reducer';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

export function initializeApp( config: AppConfig ) {
  return () => config.load();
}
export function RestangularConfigFactory ( RestangularProvider: any ) {
}

export let appConfiguration: any;

@NgModule({
  declarations: [
    AppComponent,
    LeadsFormComponent,
    NavbarComponent,
    WidgetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    StoreModule.forRoot( { appState: reducer } ),
    NgbModule,
    FormsModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [
    AppConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfig], multi: true
    }
   ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor () {
  }
}
